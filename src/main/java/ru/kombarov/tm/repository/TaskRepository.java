package ru.kombarov.tm.repository;

import ru.kombarov.tm.entity.Task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskRepository {

    private Map<String, Task> taskMap = new HashMap<>();

    public List<Task> findAll() {
        List<Task> list = new ArrayList<Task>(taskMap.values());
        return list;
    }

    public Task findOne(String id) {
        return taskMap.get(id);
    }

    public void persist(Task task) {
        taskMap.put(task.getId(), task);
    }

    public void merge(Task task) {
        if (taskMap.containsKey(task.getId())) {
            taskMap.remove(task.getId());
            taskMap.put(task.getId(), task);
        }
        else {
            taskMap.put(task.getId(), task);
        }
    }

    public void remove(String id) {
        taskMap.remove(id);
    }

    public void removeAll() {
        taskMap.clear();
    }
}
