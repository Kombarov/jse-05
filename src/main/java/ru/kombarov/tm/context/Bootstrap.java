package ru.kombarov.tm.context;

import ru.kombarov.tm.command.*;
import ru.kombarov.tm.entity.Project;
import ru.kombarov.tm.entity.Task;
import ru.kombarov.tm.repository.ProjectRepository;
import ru.kombarov.tm.repository.TaskRepository;
import ru.kombarov.tm.service.ProjectService;
import ru.kombarov.tm.service.TaskService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Bootstrap {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskService taskService = new TaskService(taskRepository);

    private final BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void init() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        registry(new HelpCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectEditCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveCommand());
        registry(new ProjectSelectCommand());
        registry(new TaskAttachCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskEditCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveCommand());
        registry(new TaskSelectCommand());
        registry(new TaskShowByProjectCommand());
        registry(new TaskUnattachCommand());
        String command = "";
        while (!"exit".equals(command)) {
            command = input.readLine();
            execute(command);
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

    public void registry(final AbstractCommand command) throws Exception {
        final String cliCommand = command.command();
        final String cliDescription = command.description();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new Exception();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new Exception();
        command.setBootstrap(this);
        commands.put(cliCommand, command);
    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public void printTasks(List<Task> tasks) {
        for (int i = 0; i < tasks.size(); i++) {
            System.out.println(i+1 + ". " + tasks.get(i).getName());
        }
    }

    public void printProjects(List<Project> projects) {
        for (int i = 0; i < projects.size(); i++) {
            System.out.println(i+1 + ". " + projects.get(i).getName());
        }
    }

    public void printTask(Task task) {
        System.out.println("task name: " + task.getName());
        System.out.println("task description: " + task.getDescription());
        System.out.println("start date: " + task.getDateStart());
        System.out.println("end date: " + task.getDateFinish());
    }

    public void printProject(Project project) {
        System.out.println("project name: " + project.getName());
        System.out.println("project description: " + project.getDescription());
        System.out.println("start date: " + project.getDateStart());
        System.out.println("end date: " + project.getDateFinish());
    }
}