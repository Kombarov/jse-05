package ru.kombarov.tm.command;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ProjectRemoveCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "project-remove";
    }

    @Override
    public String description() {
        return "Remove selected project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT REMOVE]");
        bootstrap.printProjects(bootstrap.getProjectService().findAll());
        System.out.println("ENTER PROJECT NAME FOR REMOVE");
        bootstrap.getProjectService().remove(bootstrap.getProjectService().getProjectIdByName(input.readLine()));
        System.out.println("[OK]");
    }
}
