package ru.kombarov.tm.command;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TaskRemoveCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "task-remove";
    }

    @Override
    public String description() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK REMOVE]");
        bootstrap.printTasks(bootstrap.getTaskService().findAll());
        System.out.println("ENTER TASK NAME FOR REMOVE");
        bootstrap.getTaskService().remove(bootstrap.getTaskService().getTaskIdByName(input.readLine()));
        System.out.println("[OK]");
    }
}
