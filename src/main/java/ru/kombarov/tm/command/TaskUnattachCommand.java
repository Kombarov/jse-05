package ru.kombarov.tm.command;

import ru.kombarov.tm.entity.Task;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TaskUnattachCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "task-unattach";
    }

    @Override
    public String description() {
        return "Unattach task from the project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK UNATTACH]");
        bootstrap.printTasks(bootstrap.getTaskService().findAll());
        System.out.println("ENTER TASK NAME");
        String taskName = input.readLine();
        Task task = bootstrap.getTaskService().findOne(bootstrap.getTaskService().getTaskIdByName(taskName));
        task.setProjectId("");
        System.out.println("[OK]");
    }
}
