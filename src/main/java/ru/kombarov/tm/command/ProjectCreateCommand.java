package ru.kombarov.tm.command;

import ru.kombarov.tm.entity.Project;
import ru.kombarov.tm.util.DateUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ProjectCreateCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    DateUtil dateUtil = new DateUtil();

    @Override
    public String command() {
        return "project-create";
    }

    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER PROJECT NAME");
        Project project = new Project(input.readLine());
        System.out.println("ENTER PROJECT DESCRIPTION");
        project.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        project.setDateStart(dateUtil.parseStringToDate(input.readLine()));
        System.out.println("ENTER FINISH DATE");
        project.setDateFinish(dateUtil.parseStringToDate(input.readLine()));
        bootstrap.getProjectService().persist(project);
        System.out.println("[OK]");
    }
}