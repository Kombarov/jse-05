package ru.kombarov.tm.command;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TaskSelectCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "task-select";
    }

    @Override
    public String description() {
        return "Select the task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK SELECT]");
        bootstrap.printTasks(bootstrap.getTaskService().findAll());
        System.out.println("ENTER TASK NAME");
        bootstrap.printTask(bootstrap.getTaskService().findOne(bootstrap.getTaskService().getTaskIdByName(input.readLine())));
        System.out.println("[OK]");
    }
}
