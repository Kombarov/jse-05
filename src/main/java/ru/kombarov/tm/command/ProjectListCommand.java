package ru.kombarov.tm.command;

public class ProjectListCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-list";
    }

    @Override
    public String description() {
        return "Show all projects.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST]");
        bootstrap.printProjects(bootstrap.getProjectService().findAll());
        System.out.println("[OK]");
    }
}
