package ru.kombarov.tm.command;

import ru.kombarov.tm.entity.Project;
import ru.kombarov.tm.util.DateUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ProjectEditCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    DateUtil dateUtil = new DateUtil();

    @Override
    public String command() {
        return "project-edit";
    }

    @Override
    public String description() {
        return "Edit selected project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT EDIT]");
        bootstrap.printProjects(bootstrap.getProjectService().findAll());
        System.out.println("ENTER PROJECT NAME FOR EDIT");
        String nameAnotherProject = input.readLine();
        Project anotherProject = new Project(nameAnotherProject);
        System.out.println("ENTER PROJECT DESCRIPTION");
        anotherProject.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        anotherProject.setDateStart(dateUtil.parseStringToDate(input.readLine()));
        System.out.println("ENTER FINISH DATE");
        anotherProject.setDateFinish(dateUtil.parseStringToDate(input.readLine()));
        anotherProject.setId(bootstrap.getProjectService().getProjectIdByName(nameAnotherProject));
        bootstrap.getProjectService().merge(anotherProject);
        System.out.println("[OK]");
    }
}
