package ru.kombarov.tm.command;

public class TaskListCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-list";
    }

    @Override
    public String description() {
        return "Show all tasks.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST]");
        bootstrap.printTasks(bootstrap.getTaskService().findAll());
        System.out.println("[OK]");
    }
}
