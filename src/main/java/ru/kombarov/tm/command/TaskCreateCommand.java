package ru.kombarov.tm.command;

import ru.kombarov.tm.entity.Task;
import ru.kombarov.tm.util.DateUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TaskCreateCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    DateUtil dateUtil = new DateUtil();

    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER TASK NAME");
        Task task = new Task(input.readLine());
        System.out.println("ENTER TASK DESCRIPTION");
        task.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        task.setDateStart(dateUtil.parseStringToDate(input.readLine()));
        System.out.println("ENTER FINISH DATE");
        task.setDateFinish(dateUtil.parseStringToDate(input.readLine()));
        bootstrap.getTaskService().persist(task);
        System.out.println("[OK]");
    }
}
