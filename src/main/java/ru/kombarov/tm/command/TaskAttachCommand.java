package ru.kombarov.tm.command;

import ru.kombarov.tm.entity.Task;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TaskAttachCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "task-attach";
    }

    @Override
    public String description() {
        return "Attach task to project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK ATTACH]");
        bootstrap.printTasks(bootstrap.getTaskService().findAll());
        System.out.println("ENTER TASK NAME");
        String taskName = input.readLine();
        bootstrap.printProjects(bootstrap.getProjectService().findAll());
        System.out.println("ENTER PROJECT NAME TO ATTACH TASK");
        String projectName = input.readLine();
        Task task = bootstrap.getTaskService().findOne(bootstrap.getTaskService().getTaskIdByName(taskName));
        task.setProjectId(bootstrap.getProjectService().getProjectIdByName(projectName));
        System.out.println("[OK]");
    }
}
