package ru.kombarov.tm.command;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TaskShowByProjectCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "task-show by project";
    }

    @Override
    public String description() {
        return "Show tasks by project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK SHOW BY PROJECT]");
        bootstrap.printProjects(bootstrap.getProjectService().findAll());
        System.out.println("ENTER PROJECT NAME");
        bootstrap.printTasks(bootstrap.getTaskService().getTasksByProjectId(bootstrap.getProjectService().getProjectIdByName(input.readLine())));
        System.out.println("[OK]");
    }
}
