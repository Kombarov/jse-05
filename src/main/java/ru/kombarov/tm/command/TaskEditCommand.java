package ru.kombarov.tm.command;

import ru.kombarov.tm.entity.Task;
import ru.kombarov.tm.util.DateUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TaskEditCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    DateUtil dateUtil = new DateUtil();

    @Override
    public String command() {
        return "task-edit";
    }

    @Override
    public String description() {
        return "Edit selected task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK EDIT]");
        bootstrap.printTasks(bootstrap.getTaskService().findAll());
        System.out.println("ENTER TASK NAME FOR EDIT");
        String nameAnotherTask = input.readLine();
        Task anotherTask = new Task(nameAnotherTask);
        System.out.println("ENTER TASK DESCRIPTION");
        anotherTask.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        anotherTask.setDateStart(dateUtil.parseStringToDate(input.readLine()));
        System.out.println("ENTER FINISH DATE");
        anotherTask.setDateFinish(dateUtil.parseStringToDate(input.readLine()));
        anotherTask.setId(bootstrap.getTaskService().getTaskIdByName(nameAnotherTask));
        bootstrap.getTaskService().merge(anotherTask);
        System.out.println("[OK]");
    }
}
