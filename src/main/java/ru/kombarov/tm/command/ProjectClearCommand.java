package ru.kombarov.tm.command;

public class ProjectClearCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CLEAR]");
        bootstrap.getProjectService().removeAll();
        System.out.println("[OK]");
    }
}
