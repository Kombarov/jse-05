package ru.kombarov.tm.command;

public class TaskClearCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CLEAR]");
        bootstrap.getTaskService().removeAll();
        System.out.println("[OK]");
    }
}
