package ru.kombarov.tm.command;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ProjectSelectCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "project-select";
    }

    @Override
    public String description() {
        return "Select the project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT SELECT]");
        bootstrap.printProjects(bootstrap.getProjectService().findAll());
        System.out.println("ENTER PROJECT NAME");
        bootstrap.printProject(bootstrap.getProjectService().findOne(bootstrap.getProjectService().getProjectIdByName(input.readLine())));
        System.out.println("[OK]");
    }
}
